/*
 * cdata.h: arquivo de inclus�o de uso apenas na gera��o da libpithread
 *
 * Esse arquivo pode ser modificado. ENTRETANTO, deve ser utilizada a TCB fornecida.
 *
 */
#ifndef __cdata__
#define __cdata__



#include <ucontext.h>
#include <stdlib.h>
#include <stdio.h>
#include  <math.h>
#include "support.h"


#define	PROCST_CRIACAO	0
#define	PROCST_APTO	1
#define	PROCST_EXEC	2
#define	PROCST_BLOQ	3
#define	PROCST_TERMINO	4


/* N�O ALTERAR ESSA struct */
typedef struct s_TCB { 
	int		tid; 		// identificador da thread
	int		state;		// estado em que a thread se encontra
					// 0: Cria��o; 1: Apto; 2: Execu��o; 3: Bloqueado e 4: T�rmino
        int		ticket;		// 0-255: bilhete de loteria da thread
	ucontext_t 	context;	// contexto de execu��o da thread (SP, PC, GPRs e recursos) 
} TCB_t;

typedef struct ListaWait{ // Criada para guardar a tid da thread que est� sendo bloqueada e no tid espera � guardado o tid da thread que desbloqueia ela
	int tidThread; // tid thread bloqueada
	int tidEspera; // tid thread que desbloqueia a de cima
}lWait;

void scheduler(); 
void inicializa();
void nextThread();
void exclude();
void excludeBlock();
int SearchFilaEspera();
void SwapBlockApto(int);
int threadTerminada(int);
ucontext_t *novoContexto();
int Escolha(int, int, int, int, int);


FILA2 ListaEspera;
FILA2 FilaBloqueado;
ucontext_t *schedulerContext;
TCB_t *threadExec;
int inicializar;
int tidAtual;
FILA2 FilaAptos;
FILA2 FilaFim;


#endif
