#include "../include/cdata.h"

// sudushdhdudhsudd




void scheduler(){
	getcontext(schedulerContext);
	int tidBlock;
	//Testa se chegou via yeld ou fim de thread

	while(FirstFila2(&FilaAptos) == 0)
	{
		if(threadExec -> state == PROCST_EXEC) //Terminou a thread
		{
			threadExec -> state = PROCST_TERMINO;
			AppendFila2(&FilaFim, (void *) threadExec);
			tidBlock = SearchFilaEspera();
			if(tidBlock >=0){
				SwapBlockApto(tidBlock);}
		}
		nextThread();
		exclude();
		threadExec -> state = PROCST_EXEC;
		swapcontext(schedulerContext, &threadExec->context);
	}
}




void inicializa() // Iniciliza as estruturas necessárias para o funcionamento ( thread main e scheduler)
{
	CreateFila2(&FilaBloqueado);
	CreateFila2(&ListaEspera);
	CreateFila2(&FilaAptos);
	CreateFila2(&FilaFim);
	inicializar = 1;
	//Main
	threadExec = (TCB_t *)malloc(sizeof(TCB_t));
	threadExec->tid = 0;
	tidAtual = tidAtual+1;
	//getcontext(&threadExec->context);
	threadExec->context.uc_stack.ss_sp = (char*) malloc (SIGSTKSZ);
	threadExec->context.uc_stack.ss_size = SIGSTKSZ;
	threadExec->context.uc_link = NULL;
	//Setar o Scheduler
	schedulerContext = novoContexto();
	schedulerContext -> uc_link = &threadExec -> context;
	getcontext(schedulerContext);
	makecontext(schedulerContext, (void (*)(void))scheduler, 0);

	getcontext(&threadExec->context);
}


void nextThread() // Responsave por devolver ao escalonador a thread que vai ser executada
{
	TCB_t *threadAtual, *ultimaLista, *threadSelecionada;
	int ticket, escolha, teste;
	do
	{
		ticket = Random2();
	}while(ticket>=256);
	FirstFila2(&FilaAptos);
	threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaAptos);
	LastFila2(&FilaAptos);
	ultimaLista = (TCB_t *)GetAtIteratorFila2(&FilaAptos);
	FirstFila2(&FilaAptos);
	if(threadAtual -> tid == ultimaLista->tid){
		threadSelecionada = (TCB_t *) malloc (sizeof(TCB_t));
		threadSelecionada -> tid = threadAtual -> tid;
		threadSelecionada -> state = threadAtual -> state;
		threadSelecionada -> ticket = threadAtual -> ticket;
		threadSelecionada -> context = threadAtual -> context;
	}
	else{
		threadSelecionada = (TCB_t *) malloc (sizeof(TCB_t)); // parte do principio que o primeiro elemento da lista é o selecionado e depois percorre pra procurar outro melhor
		threadSelecionada -> tid = threadAtual -> tid;
		threadSelecionada -> state = threadAtual -> state;
		threadSelecionada -> ticket = threadAtual -> ticket;
		threadSelecionada -> context = threadAtual -> context;
		do{
			NextFila2(&FilaAptos);
			threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaAptos);
			escolha = Escolha(ticket, threadSelecionada -> ticket, threadAtual -> ticket, threadSelecionada -> tid, threadAtual -> tid); // devolve a escolha de qual é a melhor thread
				if( escolha == 2)
				{
					threadSelecionada -> tid = threadAtual -> tid;
					threadSelecionada -> state = threadAtual -> state;
					threadSelecionada -> ticket = threadAtual -> ticket;
					threadSelecionada -> context = threadAtual -> context;
				}
		}while(threadAtual -> tid != ultimaLista ->tid); // controla a chegada do final da lista
	}
	threadExec = threadSelecionada;
}

void exclude() //Exclui da lista de Aptos pois a thread vai ser executada
{
	int find = 0;
	TCB_t *threadAtual;
	FirstFila2(&FilaAptos);
	do{
		threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaAptos);
		if(threadAtual -> tid == threadExec -> tid){
			DeleteAtIteratorFila2(&FilaAptos);
			find = 1;
		}
		else{
			NextFila2(&FilaAptos);}
	}while(find == 0);
}

int SearchFilaEspera(){ // verifica se a thread que terminou desbloqueia alguma outra, caso positivo ele devolve o tid da thread que está bloqueada
	lWait *Search, *Ultimo;
	LastFila2(&ListaEspera);
	Ultimo = (lWait *)GetAtIteratorFila2(&ListaEspera);

	if(FirstFila2(&ListaEspera) != 0)
		return -1;
	else{
		Search = (lWait *)GetAtIteratorFila2(&ListaEspera);
		if (Search -> tidEspera == threadExec -> tid){ // se já for o primeiro devolve tid que deve ser desbloqueada e exclui da lista, senao percorre procurando
			DeleteAtIteratorFila2(&ListaEspera);
			return Search -> tidThread;
		}else{
			while(Search->tidThread != Ultimo -> tidThread){
				NextFila2(&ListaEspera);
				Search = (lWait *)GetAtIteratorFila2(&ListaEspera);
				if (Search -> tidEspera == threadExec -> tid){
					DeleteAtIteratorFila2(&ListaEspera);
					return Search -> tidThread; // devolve o tid se achou thread para desbloquear ou -1 em caso negativo
				}
			}
		return -1;
		}
	}
}


void SwapBlockApto(int tid){ // somente move thread dos bloqueados para aptos
	TCB_t *threadAtual, *Ultima;
	LastFila2(&FilaBloqueado);
	Ultima = (TCB_t *)GetAtIteratorFila2(&FilaBloqueado);
	FirstFila2(&FilaBloqueado);
	threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaBloqueado);
	if(threadAtual -> tid == tid){
		DeleteAtIteratorFila2(&FilaBloqueado);
		threadAtual -> state = PROCST_APTO;
		AppendFila2(&FilaAptos, (void *) threadAtual);
	}
	while(threadAtual -> tid != Ultima -> tid ){
		NextFila2(&FilaBloqueado);
		threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaBloqueado);
		if(threadAtual -> tid == tid){
			DeleteAtIteratorFila2(&FilaBloqueado);
			threadAtual -> state = PROCST_APTO;
			AppendFila2(&FilaAptos, (void *) threadAtual);
		}
	}
}


int threadTerminada(int tid) // Retorna -1 no caso do cjoin tentar esperar por uma thread que já terminou, o que é um caso inválido, e 0 no caso válido
{ // Percorre toda Lista de thread que já terminaram buscando pelo tid que o cjoin recebeu como parâmetro.
	TCB_t *threadAtual, *threadUltima;
	if(FirstFila2(&FilaFim) != 0)
		return 0;
	else{
		FirstFila2(&FilaFim);
		threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaFim);
		LastFila2(&FilaFim);
		threadUltima = (TCB_t *)GetAtIteratorFila2(&FilaFim);
		if(threadAtual -> tid == threadUltima->tid){
			if(threadAtual -> tid == tid)
				return -1;
		}else{
			do{
				NextFila2(&FilaFim);
				threadAtual = (TCB_t *)GetAtIteratorFila2(&FilaFim);
				if(threadAtual -> tid == tid)
					return -1;
			}while(threadAtual -> tid != threadUltima ->tid);
		}
		return 0;
	}
}




ucontext_t *novoContexto() // criar contexto pro scheduler
{
	ucontext_t *contexto;
	contexto = (ucontext_t *) malloc (sizeof(ucontext_t));
	contexto ->uc_stack.ss_sp = (char *) malloc (SIGSTKSZ);
	contexto ->uc_stack.ss_size = SIGSTKSZ;
	return contexto;
}


int Escolha(int ticket, int ticket1, int ticket2, int tid1, int tid2){ // Calcula qual o melhor candidato pra mandar para scheduler entre o anterior e a thread do apto que ele está considerando no momento
	int escolha; // escolha 1 continua com a anterior e 2 troca para nova
	if(abs(ticket1 - ticket) == abs(ticket2 - ticket)){
		if(tid2 < tid1)
			escolha = 2;
		else
			escolha = 1;
	}
	else{
		if(abs(ticket1 - ticket) < abs(ticket2 - ticket))
			escolha = 1;
		else
			escolha = 2;
	}
	return escolha;
}
