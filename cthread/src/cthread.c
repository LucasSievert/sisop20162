
#include "../include/cthread.h"





int ccreate(void* (*start)(void*), void *arg){
	int verifica, ticket;
	if(inicializar == 0)
		inicializa(); // inicializa estruturas quando da primeira chamada
	TCB_t *newThread;
	newThread = (TCB_t*) malloc(sizeof(TCB_t));
	do{
		ticket = Random2();
	}while(ticket>=255);
	newThread->ticket = ticket;
	newThread->state = PROCST_CRIACAO;
	newThread->context.uc_stack.ss_sp = (char*) malloc (SIGSTKSZ); // criação de area de pilha exclusiva para a thread
	newThread->context.uc_stack.ss_size = SIGSTKSZ;
	newThread -> context.uc_link = schedulerContext; // ponto de volta após a conclusão da thread
	newThread->tid = tidAtual;
	tidAtual++; // Variavel global que controla o tid que é recebido por cada thread
	newThread->state = PROCST_APTO;
	getcontext(&newThread->context);
	makecontext(&newThread->context, (void (*) (void))start, 1, arg); // Contexto da thread é setado para a função que ela deve desempenhar
	verifica = AppendFila2(&FilaAptos, (void *) newThread); // Coloca na fila de Aptos
	if(verifica != 0)
		return -1;
	return newThread -> tid;
	
}

int cyield(void)
{
	int verifica; // verifica retorno do Append
	if(inicializar == 0)
	{
		inicializa();
		return 0;
	}
	threadExec -> state = PROCST_APTO; // troca Estado Atual
	verifica = AppendFila2(&FilaAptos, (void *) threadExec); // Volta para fila de Aptos
	if(verifica != 0)
		return -1;
	swapcontext(&threadExec->context, schedulerContext); // Volta Scheduler
	return 0;
}

int cjoin(int tid)
{
	lWait *Novo; // estrututa criada que guarda a tid da thread que está sendo trancada e a tid de quem libera ela
	int verifica;
	TCB_t *threadAtual;
	if(inicializar == 0)
	{
		inicializa();
		return 0;
	}
	if(tid<=tidAtual){ // Cjoin só é feito se a tid recebida como parametro já foi atribuida a alguem
		if (threadTerminada(tid) == 0){ // e se a tid não pertence a uma thread que já terminou
			if(threadExec -> tid == tid) // uma thread não pode se autobloquear
				return -1;
			if(FirstFila2(&ListaEspera) != 0){ // Se não tem ninguém bloqueado por join, ele adiciona a thread nos bloqueados e na lista de espera que contem os tid's 
				Novo = (lWait *)malloc(sizeof(lWait));
				Novo -> tidThread = threadExec -> tid;
				Novo -> tidEspera = tid;
				threadExec -> state = PROCST_BLOQ;
				AppendFila2(&FilaBloqueado, (void *) threadExec);		
				AppendFila2(&ListaEspera, (void *) Novo);
				swapcontext(&threadExec->context, schedulerContext);
				return 0;
			}
			else{
				while(NextFila2(&ListaEspera) == 0){ // percorre toda lista de espera para ver se a thread que ele quer esperar já não está sendo esperada por outra, -1 em caso de erro
					threadAtual = (TCB_t *)GetAtIteratorFila2(&ListaEspera);
					if (tid == threadAtual -> tid);
						return -1;
				} // senão vai para bloqueado e entra na lista de espera
				Novo = (lWait *)malloc(sizeof(lWait));
				Novo -> tidThread = threadExec -> tid;
				Novo -> tidEspera = tid;
				threadExec -> state = PROCST_BLOQ;
				AppendFila2(&FilaBloqueado, (void *) threadExec);
				AppendFila2(&ListaEspera, (void *) Novo);
				swapcontext(&threadExec->context, schedulerContext);
	    			return 0;
	    		}
		}
		return -1;
	}
	return -1;
}

int csem_init(csem_t *sem, int count) // Inicializa o semaforo
{
	if(count <0)
		return -1;
	sem->count = count;
	sem->fila = malloc(sizeof(FILA2));
	CreateFila2(sem->fila);
	return 0;
}
		
	
int cwait(csem_t *sem)
{	
	int verifica=0;
	if(sem == NULL) // Erro caso não tenha sido inicializado antes
		return -1;
	if( sem-> count >=1){ // Ainda pode entrar na area controlada por ele
		sem -> count = sem -> count - 1;
		return 0;
	}
	else{
		do{ 
			if(sem-> count > 0){
				verifica = 1;
			}
			else{ // Caso contrario a thread fica bloqueada até o recurso estar disponível para ela, o Loop é para garantir que após a volta do contexto ela possa testar o recurso outra vez
				threadExec -> state = PROCST_BLOQ;
				AppendFila2(sem->fila, (void *) threadExec);
				swapcontext(&threadExec->context, schedulerContext);
			}
		}while(verifica == 0);
	}
	return 0;
}
	
	
int csignal(csem_t *sem)
{
	TCB_t *novaThread;
	if (sem == NULL) // Erro caso não tenha sido inicializado antes
		return -1;
	sem -> count = sem ->count+1;
	if(sem -> count > 0 ){
		if(FirstFila2(sem->fila) == 0){ // Caso tenha alguem na fila do semaforo e o recurso está disponível, o primeiro da fila é colocado no estado de apto e sai da fila do semáforo
			novaThread = (TCB_t *)GetAtIteratorFila2(sem->fila);
			novaThread -> state = PROCST_APTO;
			AppendFila2(&FilaAptos, (void *) novaThread);
			FirstFila2(sem->fila);
			DeleteAtIteratorFila2(sem->fila);
		}
	}
	return 0;
}


int cidentify (char *name, int size){

	name = (char *) malloc(size+1*(sizeof(char)));
	name = ("Lucas Sievert da Silva 194287 \n Cassio de Abreu Ramos 193028\0");
	puts(name);
	return 0;
}

		
			
		
